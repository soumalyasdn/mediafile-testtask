import { Button, Col, Row, Table } from "antd";
import { observer } from "mobx-react";
import { useEffect, useState } from "react";
import testtranscript from '../assets/testtranscript.json';

const Dashboard = observer(() => {
    const [fetchData, setFetchData] = useState([])
    const [rowData, setRowData] = useState([])
    const { Column, ColumnGroup } = Table;

    const getSegmentData = (segmentName) => {
        var tempData = fetchData.filter(x => x.type === segmentName)
        setRowData(tempData)
    }

    useEffect(async () => {
        var tempData = []
        testtranscript.transcript.map((item) => {
            tempData.push(item)
        })
        setFetchData(tempData)
        await getSegmentData('problem')
    }, [testtranscript])

    return (
        <Row>
            <Col span={18}>
                <Table dataSource={rowData} pagination={false}>
                    <ColumnGroup title="TimeLine">
                        <Column dataIndex="start" key="start" title="Start" />
                        <Column dataIndex="end" key="end" title="End" />
                    </ColumnGroup>
                    <Column dataIndex="speaker" key="speaker" title="Speaker" />
                    <Column dataIndex="content" key="content" title="Content" />
                </Table>
            </Col>
            <Col span={6}>
                <div className="text-center">
                    <h2>Sagemnts</h2>
                    <Row>
                        <Col span={12}>
                            <Button style={{ borderRadius: '15px'}} onClick={() => getSegmentData('problem')}>Problems</Button>
                        </Col>
                        <Col span={12}>
                            <Button style={{ borderRadius: '15px' }} onClick={() => getSegmentData('symptom')}>Symptoms</Button>
                        </Col>
                    </Row>

                    <Row style={{ marginTop: '20px' }}>
                        <Col span={12}>
                            <Button style={{ borderRadius: '15px' }} onClick={() => getSegmentData('medication')}>Medications</Button>
                        </Col>
                        <Col span={12}>
                            <Button style={{ borderRadius: '15px' }} onClick={() => getSegmentData('history')}>History</Button>
                        </Col>
                    </Row>
                </div>
            </Col>
        </Row>
    )
})

export default Dashboard