import { useEffect } from "react"
import useStore from "./store"
import { BrowserRouter } from 'react-router-dom';
import AppRouter from "./utils/AppRouter";

const App = () => {
  const { AUTH } = useStore()

  useEffect(() => {
    AUTH.setAxiosInterceptors()
  }, [AUTH])

  return (
    <BrowserRouter basename='/'>
      <AppRouter />
    </BrowserRouter>
  )

}

export default App