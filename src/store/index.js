import { createContext, useContext } from "react";
import AuthStore from './AuthStore';

const AppContext = createContext({
    AUTH: new AuthStore(),
})

const useStore = () => useContext(AppContext)

export default useStore