import axios from 'axios'
import { makeAutoObservable } from 'mobx'
import Notify from '../utils/Notify'

export default class AuthStore {
    loading = true
    token = null
    constructor() {
        makeAutoObservable(this)
        this.initiatAppOptions()
    }

    setAxiosDefaults = () => {
        axios.defaults.baseURL = process.env.React_APP_API_URL
    }

    setAxiosInterceptors = () => {
        axios.interceptors.response.use(data => {
            return data
        }, data => {
            if (data.response) {
                switch (data.response.status) {
                    case 401:
                        localStorage.clear()
                        break;

                    case 403:
                        break;

                    default:
                        Notify.error({ message: `Something went wrong. For more detail [${data.response.status}]` })
                        break;
                }
            } else {
                Notify.success({ message: "Network Problem" })
                data.response = { data: {} }
            }
        })
    }

    //still have to set token
    initiatAppOptions = () => {
        this.loading = true
        this.setAxiosDefaults()
        let token = 'setTokenHere'
        if (token && token !== 'undefined') {
            axios.defaults.headers = { Accept: 'application/json', Authorization: 'Bearer ' + token }
        } else {
            axios.defaults.headers = { Accept: 'application/json' }
            this.setUser(null, null)
            this.loading = false
        }
    }

}