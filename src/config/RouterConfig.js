import Dashboard from '../pages/Dashboard';

export const RouterConfig = [
    {
        title: 'Dashboard',
        path: '/',
        component: Dashboard,
        auth: true,
        default: 'afterAuth'
    }
]